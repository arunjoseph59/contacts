# README #

Created this project as part of learning purpose and content creation.

### What is this repository for? ###

* A mock Contact App for with Static data
* Swift, SwiftUI, iOS

### Details

* Version 1.0
* iOS 14 and above.
* Potrait only

### Connect with me on Linked In
* https://www.linkedin.com/in/arun-joseph-8a97a2165

