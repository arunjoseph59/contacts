//
//  Created by Arun Joseph
//  @Arun_Jo
//

import Foundation

class ContactViewMdoel: ObservableObject {
    
    @Published var contacts: [ContactData] = []
    @Published var missedCalls: [ContactData] = []
    
    init() {
        let contacts = [
            ContactData(personImage: "", personName: "Abraham Ezra", deviceType: .mobile, day: .monday, isMissed: false, temporaryColor: .yellow, phoneNumber: "4434-09-8978", calls: Calls(incomingCallsCount: "4", outgoingCallsCount: "2", missedCallsCount: "1")),
            ContactData(personImage: "", personName: "Adam Johan", deviceType: .mobile, day: .tuesday, isMissed: true, temporaryColor: .green, phoneNumber: "4456-65-7590", calls: Calls(incomingCallsCount: "4", outgoingCallsCount: "17", missedCallsCount: "12")),
            ContactData(personImage: "", personName: "Zairah Eve", deviceType: .mobile, day: .wednesday, isMissed: false, temporaryColor: .orange, phoneNumber: "9087-78-9009", calls: Calls(incomingCallsCount: "3", outgoingCallsCount: "2", missedCallsCount: "0")),
            ContactData(personImage: "", personName: "Eiza Maria", deviceType: .mobile, day: .thursday, isMissed: true, temporaryColor: .pink, phoneNumber: "4789-90-9880", calls: Calls(incomingCallsCount: "0", outgoingCallsCount: "2", missedCallsCount: "4")),
            ContactData(personImage: "", personName: "Claire", deviceType: .mobile, day: .friday, isMissed: false, temporaryColor: .yellow, phoneNumber: "4409-98-3412", calls: Calls(incomingCallsCount: "1", outgoingCallsCount: "0", missedCallsCount: "9")),
            ContactData(personImage: "", personName: "Asanda John", deviceType: .mobile, day: .saturday, isMissed: true, temporaryColor: .purple, phoneNumber: "4478-90-8976", calls: Calls(incomingCallsCount: "0", outgoingCallsCount: "4", missedCallsCount: "0")),
            ContactData(personImage: "", personName: "Chris Morgan", deviceType: .mobile, day: .sunday, isMissed: false, temporaryColor: .secondary, phoneNumber: "9089-78-9123", calls: Calls(incomingCallsCount: "3", outgoingCallsCount: "4", missedCallsCount: "0")),
            ContactData(personImage: "", personName: "Liza", deviceType: .mobile, day: .monday, isMissed: true, temporaryColor: .pink, phoneNumber: "0876-09-3471", calls: Calls(incomingCallsCount: "4", outgoingCallsCount: "0", missedCallsCount: "2")),
            ContactData(personImage: "", personName: "Harry Kane", deviceType: .mobile, day: .tuesday, isMissed: false, temporaryColor: .orange, phoneNumber: "4412-09-5468", calls: Calls(incomingCallsCount: "12", outgoingCallsCount: "23", missedCallsCount: "11")),
            ContactData(personImage: "", personName: "Lana Rodriguez", deviceType: .mobile, day: .wednesday, isMissed: true, temporaryColor: .green, phoneNumber: "9946-90-1104", calls: Calls(incomingCallsCount: "3", outgoingCallsCount: "4", missedCallsCount: "1")),
            ContactData(personImage: "", personName: "Steven John", deviceType: .mobile, day: .thursday, isMissed: false, temporaryColor: .pink, phoneNumber: "4409-56-1983", calls: Calls(incomingCallsCount: "9", outgoingCallsCount: "7", missedCallsCount: "6")),
            ContactData(personImage: "", personName: "Andrian Luiz", deviceType: .mobile, day: .friday, isMissed: true, temporaryColor: .orange, phoneNumber: "8089-71-9081", calls: Calls(incomingCallsCount: "6", outgoingCallsCount: "5", missedCallsCount: "4")),
            ContactData(personImage: "", personName: "Adrian Xavi", deviceType: .mobile, day: .saturday, isMissed: false, temporaryColor: .purple, phoneNumber: "4434-73-2753", calls: Calls(incomingCallsCount: "8", outgoingCallsCount: "5", missedCallsCount: "3")),
            ContactData(personImage: "", personName: "Levine Adam", deviceType: .mobile, day: .sunday, isMissed: true, temporaryColor: .pink, phoneNumber: "0987-83-2107", calls: Calls(incomingCallsCount: "3", outgoingCallsCount: "4", missedCallsCount: "14"))]
        self.contacts = contacts
    }
    
    func getMissedCalls() {
        self.missedCalls = self.contacts.filter({$0.isMissed})
    }
    
    func getContactsContainingSearchText(text: String) {
        
    }
}
