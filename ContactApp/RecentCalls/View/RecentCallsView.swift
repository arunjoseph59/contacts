//
//  Created by Arun Joseph
//  @Arun_Jo
//
import SwiftUI

struct RecentCallsView: View {
    
    @State var serchingText: String = ""
    @State var callType: CallType = .all
    @StateObject var viewModel = ContactViewMdoel()
    
    init() {
        UITableView.appearance().backgroundColor = UIColor.black
        UITableView.appearance().showsVerticalScrollIndicator = false
        UITableViewCell.appearance().selectionStyle = .none
    }
    
    var body: some View {
        NavigationView {
            ZStack {
                Color.black
                    .edgesIgnoringSafeArea(.all)
                
                VStack {
                    
                    //MARK: All and Missed button HSTack
                    HStack(spacing: 20.0){
                        Spacer()
                        Button(action: {
                            self.callType = .all
                        })
                        {
                            RoundedRectangle(cornerRadius: 30.0)
                                .stroke(Color.white, lineWidth: 2.0)
                                .frame(width: 140.0, height: 60.0, alignment: .center)
                                .overlay(
                                    Text("All")
                                        .shadow(radius: 8.0)
                                        .foregroundColor(.white)
                                        .font(.system(size: 20.0, weight: .bold))
                                )
                                .transition(.slide)
                        }
                        .background(self.callType == .all ? Color.blue: Color.clear)
                        .clipShape(RoundedRectangle(cornerRadius: 29.0))
                        .opacity(0.9)
                        
                        Button(action: {
                            self.callType = .missed
                            self.viewModel.getMissedCalls()
                        })
                        {
                            RoundedRectangle(cornerRadius: 30.0)
                                .stroke(Color.white, lineWidth: 2.0)
                                .frame(width: 140.0, height: 60.0, alignment: .center)
                                .overlay(
                                    Text("Missed")
                                        .shadow(radius: 8.0)
                                        .foregroundColor(.white)
                                        .font(.system(size: 20.0, weight: .bold))
                                )
                                .transition(.slide)
                        }
                        .background(self.callType == .missed ? Color.blue: Color.clear)
                        .clipShape(RoundedRectangle(cornerRadius: 29.0))
                        .opacity(0.9)
                        Spacer()
                    }
                    .animation(.linear)
                    
                    Spacer()
                        .frame(height: 50.0)
                    
                    //MARK: Recemts and Edit HStack
                    HStack {
                        Text("Recents")
                            .foregroundColor(.white)
                            .font(.system(size: 30.0, weight: .heavy))
                        Spacer()
                        Button(action:  {
                            
                        }){
                            Image(systemName: "square.and.pencil")
                                .font(.system(size: 35.0))
                                .foregroundColor(.white)
                                .opacity(0.6)
                        }
                    }
                    
                    Spacer()
                        .frame(height: 50.0)
                    
                    //MARK: Search
                    ZStack {
                        RoundedRectangle(cornerRadius: 25.0)
                            .stroke(Color.white, lineWidth: 2.0)
                            .opacity(0.6)
                            .frame(height: 50.0)
                            .foregroundColor(.clear)
                        HStack {
                            Image(systemName: "magnifyingglass")
                                .font(.system(size: 25.0))
                            TextField("", text: $serchingText)
                                .frame(height: 40.0)
                                .placeholder(when: self.serchingText.isEmpty, placeholder: {
                                    Text("Search").foregroundColor(.gray)
                                })
                            Button(action: {
                                self.hideKeyboard()
                                self.serchingText = ""
                            })
                            {
                                Image(systemName: "multiply")
                                    .font(.system(size: 25.0))
                                    .foregroundColor(.white)
                                    .opacity(0.6)
                            }
                        }
                        .foregroundColor(.white)
                        .opacity(0.6)
                        .padding(.horizontal, 15.0)
                    }
                    
                    Spacer()
                        .frame(height: 30.0)
                    List{
                        ForEach(callType == .all ? viewModel.contacts.filter({ (contact: ContactData)-> Bool in
                            return contact.personName.hasPrefix(serchingText)
                        }): viewModel.missedCalls.filter({ (contact: ContactData)-> Bool in
                            return contact.personName.hasPrefix(serchingText)
                        }), id: \.self) { contact in
                            NavigationLink(destination: ContactDetailsView(contact: contact), label: {
                                RecentCallsListCellView(contact: contact)
                                    .transition(.slide)
                                    .listRowInsets(EdgeInsets())
                            })
                        }.listRowBackground(Color.black)
                    }
                    .animation(.default)
                    .gesture(DragGesture()
                                .onChanged({ _ in
                                    self.hideKeyboard()
                                })
                    )
                    
                    Spacer()
                }
                .padding(.horizontal)
            }
            .navigationBarHidden(true)
//            .onTapGesture {
//                self.hideKeyboard()
//            }
        }
    }
    
    func filterContactsWithSearchText() {
        self.viewModel.getContactsContainingSearchText(text: serchingText)
    }
}

struct RecentCallsView_Previews: PreviewProvider {
    static var previews: some View {
        RecentCallsView()
    }
}

enum CallType {
    case missed
    case all
}
