//
//  Created by Arun Joseph
//  @Arun_Jo
//

import Foundation
import SwiftUI

struct RecentCallsListCellView: View {
    
    var contact: ContactData
    
    var body: some View {
        HStack {
            Circle()
                .fill(contact.temporaryColor)
                .frame(width: 85.0, height: 85.0, alignment: .center)
            Spacer()
                .frame(width: 20.0)
            VStack(alignment: .leading, spacing: 8.0){
                Text(contact.personName)
                    .font(.system(size: 20.0, weight: .bold))
                Text(contact.deviceType.rawValue)
                    .font(.system(size: 15.0))
            }
            .foregroundColor(.white)
            
            Spacer(minLength: 20.0)
            
            VStack(alignment: .trailing, spacing: 8.0) {
                Image(systemName: self.contact.isMissed ? "phone.fill.arrow.down.left": "phone.connection")
                    .font(.system(size: 20.0))
                    .foregroundColor(self.contact.isMissed ? .red: .white)
                
                Text(contact.day.rawValue)
                    .font(.system(size: 15.0))
                    .foregroundColor(.white)
            }
            
        }
        .padding(.vertical)
        .background(Color.clear)
    }
}

struct ContactListCellView_PreviewProvider: PreviewProvider {
    static var previews: some View {
        RecentCallsListCellView(contact: ContactData(personImage: "", personName: "Adrian Luiz", deviceType: .mobile, day: .friday, isMissed: true, temporaryColor: .pink, phoneNumber: "4456-09-1483", calls: Calls(incomingCallsCount: "3", outgoingCallsCount: "9", missedCallsCount: "12")))
    }
}
