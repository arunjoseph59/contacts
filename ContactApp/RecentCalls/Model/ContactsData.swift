//
//  Created by Arun Joseph
//  @Arun_Jo
//

import Foundation
import SwiftUI

struct ContactData: Hashable {
    let personImage: String
    let personName: String
    let deviceType: DeviceType
    let day: Days
    let isMissed: Bool
    let temporaryColor: Color
    let phoneNumber: String
    let calls: Calls
}

struct Calls: Hashable {
    let incomingCallsCount: String
    let outgoingCallsCount: String
    let missedCallsCount: String
}

enum Days: String {
    case sunday = "Sunday"
    case monday = "Monday"
    case tuesday = "Tuesday"
    case wednesday = "Wednesday"
    case thursday = "Thursday"
    case friday = "Friday"
    case saturday = "Saturday"
}

enum DeviceType: String {
    case mobile = "Mobile"
    case landline = "Land Phone"
}
