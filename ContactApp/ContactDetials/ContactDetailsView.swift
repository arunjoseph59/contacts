//
//  ContactDetailsView.swift
//  ContactApp
//
//  Created by A-10474 on 09/02/22.
//

import Foundation
import SwiftUI


struct ContactDetailsView: View {
    
    var selectedContact: ContactData
    @State private var selectedButton: ButtonType = .phone
    @State private var notes: String = ""
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    init(contact: ContactData) {
        self.selectedContact = contact
        
        UITextView.appearance().backgroundColor = .clear
    }
    
    var body: some View {
        ZStack {
            Color.black
                .edgesIgnoringSafeArea(.all)
            VStack {
                
                //MARK: Back and Edit HStack
                HStack {
                    
                    //MARK: Back button
                    Button(action: {
                        self.presentationMode.wrappedValue.dismiss()
                    })
                    {
                        RoundedRectangle(cornerRadius: 25.0, style: .continuous)
                            .stroke(Color.white.opacity(0.8), lineWidth: 1.5)
                            .frame(width: 50.0, height: 50.0, alignment: .center)
                            .overlay(
                                Image(systemName: "chevron.left")
                                    .font(.system(size: 25.0))
                                    .foregroundColor(.white)
                            )
                    }
                    
                    Spacer()
                    
                    //MARK: Edit Button
                    Button(action: {
                        //TODO: Edit button action
                    })
                    {
                        RoundedRectangle(cornerRadius: 30.0, style: .continuous)
                            .stroke(Color.white.opacity(0.8), lineWidth: 1.5)
                            .frame(width: 50.0, height: 50.0, alignment: .center)
                            .overlay(
                                Image(systemName: "pencil")
                                    .foregroundColor(.white)
                                    .font(.system(size: 25.0))
                            )
                    }
                }
                
                Spacer()
                    .frame(height: 25.0)
                
                ScrollView( .vertical) {
                    VStack {
                        //MARK: Image, Name and number VStack
                        VStack {
                            Image("") //TODO: need to add image
                                .resizable()
                                .frame(width: 120.0, height: 120.0, alignment: .center)
                                .background(selectedContact.temporaryColor)
                                .clipShape(Circle())
                            
                            Spacer()
                                .frame(height: 20.0)
                            
                            Text(selectedContact.personName)
                                .foregroundColor(.white)
                                .font(.system(size: 30.0, weight: .heavy))
                            
                            Spacer()
                                .frame(height: 10.0)
                            
                            Text(selectedContact.phoneNumber)
                                .foregroundColor(.white)
                                .opacity(0.8)
                                .font(.system(size: 16.0))
                        }
                        
                        Spacer()
                            .frame(height: 40.0)
                        
                        //MARK: Phone, Message, FaceTime & Email HStack
                        HStack{
                            Spacer()
                            Button(action: {
                                self.selectedButton = .phone
                            })
                            {
                                RoundedRectangle(cornerRadius: 25.0, style: .continuous)
                                    .stroke(selectedButton != .phone ? Color.white.opacity(0.6): Color.clear, lineWidth: 1.5)
                                    .frame(width: 50.0, height: 50.0, alignment: .center)
                                    .transition(.slide)
                                    .overlay(
                                        Image(systemName: "phone")
                                            .font(.system(size: 25.0))
                                            .foregroundColor(.white)
                                    )
                            }
                            .background(selectedButton == .phone ? Color.blue: Color.clear)
                            .clipShape(RoundedRectangle(cornerRadius: 23.0))
                            
                            Spacer(minLength: 20.0)
                            
                            Button(action: {
                                self.selectedButton = .message
                            })
                            {
                                RoundedRectangle(cornerRadius: 25.0, style: .continuous)
                                    .stroke(selectedButton != .message ? Color.white.opacity(0.6): Color.clear, lineWidth: 1.5)
                                    .frame(width: 50.0, height: 50.0, alignment: .center)
                                    .transition(.slide)
                                    .overlay(
                                        Image(systemName: "message")
                                            .font(.system(size: 25.0))
                                            .foregroundColor(.white)
                                    )
                                
                            }
                            .background(selectedButton == .message ? Color.blue: Color.clear)
                            .clipShape(RoundedRectangle(cornerRadius: 23.0))
                            
                            Spacer(minLength: 20.0)
                            
                            Button(action: {
                                self.selectedButton = .video
                            })
                            {
                                RoundedRectangle(cornerRadius: 25.0, style: .continuous)
                                    .stroke(selectedButton != .video ? Color.white.opacity(0.6): Color.clear, lineWidth: 1.5)
                                    .frame(width: 50.0, height: 50.0, alignment: .center)
                                    .transition(.slide)
                                    .overlay(
                                        Image(systemName: "video")
                                            .font(.system(size: 25.09))
                                            .foregroundColor(.white)
                                    )
                            }
                            .background(selectedButton == .video ? Color.blue: Color.clear)
                            .clipShape(RoundedRectangle(cornerRadius: 23.0))
                            
                            Spacer(minLength: 20.0)
                            
                            Button(action: {
                                self.selectedButton = .email
                            })
                            {
                                RoundedRectangle(cornerRadius: 25.0, style: .continuous)
                                    .stroke(selectedButton == .email ? Color.clear: Color.white.opacity(0.6), lineWidth: 1.5)
                                    .frame(width: 50.0, height: 50.0, alignment: .center)
                                    .transition(.slide)
                                    .overlay(
                                        Image(systemName: "envelope")
                                            .font(.system(size: 25.0))
                                            .foregroundColor(.white)
                                    )
                            }
                            .background(selectedButton == .email ? Color.blue: Color.clear)
                            .clipShape(RoundedRectangle(cornerRadius: 23.0))
                            
                            Spacer()
                        }
                        .animation(.default)
                        
                        Spacer()
                            .frame(height: 50.0)
                        
                        HStack {
                            Text("Summary")
                                .font(.system(size: 20.0, weight: .bold))
                                .foregroundColor(.white)
                            Spacer()
                        }
                        
                        Spacer()
                            .frame(height: 30.0)
                        
                        HStack {
                            ZStack{
                                RoundedRectangle(cornerRadius: 10.0, style: .continuous)
                                    .fill(Color.white.opacity(0.1))
                                    .frame(height: 150.0)
                                
                                VStack(alignment: .center, spacing: 20.0) {
                                    Image(systemName: "phone.arrow.up.right")
                                        .font(.system(size: 25.0))
                                        .foregroundColor(.white)
                                    
                                    Text("Outgoing calls")
                                        .foregroundColor(.white)
                                        .font(.system(size: 15.0))
                                    
                                    Text(selectedContact.calls.outgoingCallsCount)
                                        .foregroundColor(.white)
                                        .font(.system(size: 15.0, weight: .bold))
                                }
                            }
                            
                            ZStack{
                                RoundedRectangle(cornerRadius: 10.0, style: .continuous)
                                    .fill(Color.white.opacity(0.1))
                                    .frame(height: 150.0)
                                
                                VStack(alignment: .center, spacing: 20.0) {
                                    Image(systemName: "phone.badge.plus")
                                        .font(.system(size: 25.0))
                                        .foregroundColor(.red)
                                    
                                    Text("Missed calls")
                                        .foregroundColor(.red)
                                        .font(.system(size: 15.0))
                                    
                                    Text(selectedContact.calls.missedCallsCount)
                                        .foregroundColor(.red)
                                        .font(.system(size: 15.0, weight: .bold))
                                }
                            }
                            
                            ZStack{
                                RoundedRectangle(cornerRadius: 10.0, style: .continuous)
                                    .fill(Color.white.opacity(0.1))
                                    .frame(height: 150.0)
                                
                                VStack(alignment: .center, spacing: 20.0) {
                                    Image(systemName: "phone.arrow.down.left")
                                        .font(.system(size: 25.0))
                                        .foregroundColor(.white)
                                    
                                    Text("Incoming calls")
                                        .foregroundColor(.white)
                                        .font(.system(size: 15.0))
                                    
                                    Text(selectedContact.calls.incomingCallsCount)
                                        .foregroundColor(.white)
                                        .font(.system(size: 15.0, weight: .bold))
                                }
                            }
                        }
                        
                        Spacer()
                            .frame(height: 25.0)
                        
                        
                        ZStack {
                            RoundedRectangle(cornerRadius: 10.0, style: .continuous)
                                .fill(Color.white.opacity(0.1))
                                .frame(height: 180.0)
                            
                            VStack{
                                HStack {
                                    Text("Notes")
                                        .font(.system(size: 18.0))
                                        .foregroundColor(.white)
                                    
                                    Spacer()
                                    
                                    Button(action: {
                                        self.notes = ""
                                        self.hideKeyboard()
                                    })
                                    {
                                        Image(systemName: "multiply")
                                            .font(.system(size: notes == "" ? 0: 25.0))
                                            .foregroundColor(.white)
                                            .padding()
                                    }
                                }
                                .frame(height: 25.0)
                                .padding(.top, 25.0)
                                .padding(.leading, 15.0)
                                
                                TextEditor(text: $notes)
                                    .foregroundColor(.white)
                                    .navigationTitle("Notes")
                                    .frame(height: 100.0)
                                    .padding(.vertical, 15.0)
                                    .padding(.horizontal, 15.0)

                            }
                        }
                        
                        Spacer()
                    }
                }
                
            }
            .padding(.vertical)
            .padding(.horizontal, 30.0)
        }
        .navigationBarHidden(true)
    }
}


//MARK: Preview Provider
struct ContactDetailsView_PreviewProvider: PreviewProvider {
    static var previews: some View {
        ContactDetailsView(contact: ContactData(personImage: "", personName: "Adam Johan", deviceType: .mobile, day: .monday, isMissed: true, temporaryColor: .pink, phoneNumber: "9734-89-9078", calls: Calls(incomingCallsCount: "3", outgoingCallsCount: "9", missedCallsCount: "12")))
    }
}


enum ButtonType {
    case phone
    case message
    case video
    case email
}
